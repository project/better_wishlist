(function (Drupal) {
    Drupal.better_wishlist = Drupal.better_wishlist || [];
    Drupal.better_wishlist.storeUuid = (uuid) => {
      var storedUuidsString = ('; ' + document.cookie).split(`; better_wishlists=`).pop().split(';')[0] || '[]';
      var storedUuids = JSON.parse(storedUuidsString);
      if (!storedUuids.includes(uuid)) {
        storedUuids.push(uuid);
        document.cookie = 'better_wishlists = ' + JSON.stringify(storedUuids)
      }
    };

    Drupal.AjaxCommands.prototype.betterWishlistStoreUuid = (ajax, response) => {
      Drupal.better_wishlist.storeUuid(response.uuid);
    }

    Drupal.behaviors.better_wishlist = {
      attach: function (context) {
        if (drupalSettings.better_wishlist) {
          var currentUuid = drupalSettings.better_wishlist.currentUuid;
          Drupal.better_wishlist.storeUuid(currentUuid);
        }
      }
    }
  }
)
(Drupal)
