<?php

/**
 * @file
 * Contains better_wishlist_item.page.inc.
 *
 * Page callback for Wishlist item entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Wishlist item templates.
 *
 * Default template: better_wishlist_item.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_better_wishlist_item(array &$variables) {
  // Fetch WishlistItem Entity Object.
  $better_wishlist_item = $variables['elements']['#better_wishlist_item'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
