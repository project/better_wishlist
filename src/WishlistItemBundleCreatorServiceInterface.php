<?php

namespace Drupal\better_wishlist;

/**
 * Interface for wishlist item bundle creators.
 */
interface WishlistItemBundleCreatorServiceInterface {

  /**
   * Checks if a wishlist item bundle exists for the given entity type.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return bool
   *   Whether the bundle exists.
   */
  public function bundleExists(string $entity_type_id): bool;

  /**
   * Creates a wishlist item bundle for the given entity type.
   *
   * @param string $entity_type_id
   *   The entity type id.
   */
  public function createBundle(string $entity_type_id): void;

}
