<?php

namespace Drupal\better_wishlist\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\better_wishlist\Entity\WishlistItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WishlistItemController.
 *
 *  Returns responses for Wishlist item routes.
 */
class WishlistItemController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Wishlist item revision.
   *
   * @param int $better_wishlist_item_revision
   *   The Wishlist item revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($better_wishlist_item_revision) {
    $better_wishlist_item = $this->entityTypeManager()
      ->getStorage('better_wishlist_item')
      ->loadRevision($better_wishlist_item_revision);
    $view_builder = $this->entityTypeManager()
      ->getViewBuilder('better_wishlist_item');

    return $view_builder->view($better_wishlist_item);
  }

  /**
   * Page title callback for a Wishlist item revision.
   *
   * @param int $better_wishlist_item_revision
   *   The Wishlist item revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($better_wishlist_item_revision) {
    $better_wishlist_item = $this->entityTypeManager()
      ->getStorage('better_wishlist_item')
      ->loadRevision($better_wishlist_item_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $better_wishlist_item->label(),
      '%date' => $this->dateFormatter->format($better_wishlist_item->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Wishlist item.
   *
   * @param \Drupal\better_wishlist\Entity\WishlistItemInterface $better_wishlist_item
   *   A Wishlist item object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(WishlistItemInterface $better_wishlist_item) {
    $account = $this->currentUser();
    $better_wishlist_item_storage = $this->entityTypeManager()
      ->getStorage('better_wishlist_item');

    $langcode = $better_wishlist_item->language()->getId();
    $langname = $better_wishlist_item->language()->getName();
    $languages = $better_wishlist_item->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', [
      '@langname' => $langname,
      '%title' => $better_wishlist_item->label(),
    ]) : $this->t('Revisions for %title', ['%title' => $better_wishlist_item->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all better_wishlist item revisions") || $account->hasPermission('administer better_wishlist item entities')));
    $delete_permission = (($account->hasPermission("delete all better_wishlist item revisions") || $account->hasPermission('administer better_wishlist item entities')));

    $rows = [];

    $vids = $better_wishlist_item_storage->revisionIds($better_wishlist_item);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\better_wishlist\WishlistItemInterface $revision */
      $revision = $better_wishlist_item_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)
        ->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $better_wishlist_item->getRevisionId()) {
          $link = $this->l($date, new Url('entity.better_wishlist_item.revision', [
            'better_wishlist_item' => $better_wishlist_item->id(),
            'better_wishlist_item_revision' => $vid,
          ]));
        }
        else {
          $link = $better_wishlist_item->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.better_wishlist_item.translation_revert', [
                'better_wishlist_item' => $better_wishlist_item->id(),
                'better_wishlist_item_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.better_wishlist_item.revision_revert', [
                'better_wishlist_item' => $better_wishlist_item->id(),
                'better_wishlist_item_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.better_wishlist_item.revision_delete', [
                'better_wishlist_item' => $better_wishlist_item->id(),
                'better_wishlist_item_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['better_wishlist_item_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
