<?php

namespace Drupal\better_wishlist;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Wishlist item entities.
 *
 * @ingroup better_wishlist
 */
class WishlistItemListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Wishlist item ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\better_wishlist\Entity\WishlistItem $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.better_wishlist_item.edit_form',
      ['better_wishlist_item' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
