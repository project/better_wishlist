<?php

namespace Drupal\better_wishlist\Plugin\EntityReferenceSelection;

use Drupal\better_wishlist\WishlistItemBundleCreatorServiceInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Entity reference selection for wishlist items. Creates wishlist item
 * bundles for selected reference types.
 *
 * @EntityReferenceSelection(
 *   id = "better_wishlist_item_type_selection",
 *   label = @Translation("Better wishlist items"),
 *   group = "better_wishlist_item_type_selection",
 *   weight = 0,
 *   deriver = "Drupal\Core\Entity\Plugin\Derivative\DefaultSelectionDeriver"
 * )
 */
class WishlistItemTypeSelection extends DefaultSelection {

  /**
   * The bundle creator.
   *
   * @var \Drupal\better_wishlist\WishlistItemBundleCreatorServiceInterface
   */
  private $bundleCreator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity.repository'),
      $container->get('better_wishlist.bundle_creator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, AccountInterface $current_user, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, EntityRepositoryInterface $entity_repository, WishlistItemBundleCreatorServiceInterface $bundle_creator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $module_handler, $current_user, $entity_field_manager, $entity_type_bundle_info, $entity_repository);
    $this->bundleCreator = $bundle_creator;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $entity_types = $this->entityTypeManager->getDefinitions();
    $type_options = [];
    foreach ($entity_types as $entity_type) {
      $type_options[$entity_type->id()] = $entity_type->getLabel();
    }

    $form["target_bundles"]["#options"] = $type_options;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $errors = $form_state->getErrors();
    if (empty($errors)) {
      $settings = $form_state->getValue('settings');
      if (empty($settings['handler_settings']) || empty($settings['handler_settings']['target_bundles'])) {
        return;
      }
      foreach ($settings['handler_settings']['target_bundles'] as $target_bundle) {
        $this->bundleCreator->createBundle($target_bundle);
      }
    }
  }

}
