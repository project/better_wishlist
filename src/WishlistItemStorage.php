<?php

namespace Drupal\better_wishlist;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for Wishlist item entities.
 *
 * This extends the base storage class, adding required special handling for
 * Wishlist item entities.
 *
 * @ingroup better_wishlist
 */
class WishlistItemStorage extends SqlContentEntityStorage implements WishlistItemStorageInterface {

}
