<?php

namespace Drupal\better_wishlist;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the storage handler class for Wishlist entities.
 *
 * This extends the base storage class, adding required special handling for
 * Wishlist entities.
 *
 * @ingroup better_wishlist
 */
interface WishlistStorageInterface extends ContentEntityStorageInterface {


}
