<?php

namespace Drupal\better_wishlist;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Wishlist entities.
 *
 * @ingroup better_wishlist
 */
class WishlistListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    $header['owner'] = $this->t('Owner');
    $header['type'] = $this->t('Type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\better_wishlist\Entity\Wishlist $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.better_wishlist.edit_form',
      ['better_wishlist' => $entity->uuid()]
    );
    $row['owner'] = $entity->getOwner()->toLink();
    $row['type'] = $entity->bundle();
    return $row + parent::buildRow($entity);
  }

}
