<?php

namespace Drupal\better_wishlist\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command for registering a new wishlist.
 */
class RegisterWishlistCommand implements CommandInterface {

  /**
   * The uuid.
   *
   * @var string
   */
  private $uuid;

  /**
   * {@inheritdoc}
   */
  public function __construct($uuid) {
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'betterWishlistStoreUuid',
      'uuid' => $this->uuid,
    ];
  }

}
