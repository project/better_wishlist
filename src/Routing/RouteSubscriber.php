<?php

namespace Drupal\better_wishlist\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber for better wishlist module.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $routes = [
      'entity.better_wishlist.canonical',
      'entity.better_wishlist.edit_form',
      'entity.better_wishlist.delete_form',
      'better_wishlist_widget.add_to_wishlist',
      'better_wishlist_widget.remove_from_wishlist',
    ];

    foreach ($routes as $route_id) {
      $route = $collection->get($route_id);
      if (!empty($route)) {
        $route->setOption('parameters', ['better_wishlist' => ['type' => 'better_wishlist']]);
        $requirements = $route->getRequirements();
        unset($requirements['better_wishlist']);
        $route->setRequirements($requirements);
      }
    }
  }

}
