<?php

namespace Drupal\better_wishlist;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\better_wishlist\Entity\WishlistItemType;

/**
 * Default wishlist item bundle creator service.
 */
class WishlistItemBundleCreatorService implements WishlistItemBundleCreatorServiceInterface {

  /**
   * {@inheritdoc}
   */
  public function bundleExists(string $entity_type_id): bool {
    $bundle = WishlistItemType::load($entity_type_id);
    return !empty($bundle);
  }

  /**
   * {@inheritdoc}
   */
  public function createBundle(string $entity_type_id): void {

    if ($this->bundleExists($entity_type_id)) {
      return;
    }

    $bundle_key = $entity_type_id;

    $bundle = WishlistItemType::create([
      'id' => $bundle_key,
      'label' => 'Wishlist item ' . $entity_type_id,
      'revision' => TRUE,
    ]);
    $bundle->save();

    $field_storage = FieldStorageConfig::loadByName('better_wishlist_item', $entity_type_id);
    if (empty($field_storage)) {
      $field_storage = FieldStorageConfig::create([
        'field_name' => $entity_type_id,
        'entity_type' => 'better_wishlist_item',
        'type' => 'entity_reference',
        'settings' => [
          'target_type' => $entity_type_id,
        ],
      ]);
      $field_storage->save();
    }

    $field_config = FieldConfig::create([
      'field_name' => $entity_type_id,
      'entity_type' => 'better_wishlist_item',
      'bundle' => $bundle_key,
      'label' => 'Wishlisted entity',
      'required' => TRUE,
    ]);
    $field_config->save();
  }

}
