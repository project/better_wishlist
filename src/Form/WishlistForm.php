<?php

namespace Drupal\better_wishlist\Form;

use Drupal\better_wishlist\Ajax\RegisterWishlistCommand;
use Drupal\better_wishlist\Ajax\ReloadWishlistsCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Wishlist edit forms.
 *
 * @ingroup better_wishlist
 */
class WishlistForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\better_wishlist\Entity\Wishlist $entity */
    $form = parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    parent::save($form, $form_state);
    $redirect = \Drupal::request()->get('redirect');
    if (!empty($redirect)) {
      $form_state->setRedirectUrl(Url::fromUserInput($redirect));
    }
    else {
      $form_state->setRedirect('entity.better_wishlist.canonical', ['better_wishlist' => $entity->uuid()]);
    }

  }

}
