<?php

namespace Drupal\better_wishlist\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\better_wishlist\WishlistItemBundleCreatorServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Admin settings for for the better wishlist module.
 */
class WishlistAdminForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The bundle creator service.
   *
   * @var \Drupal\better_wishlist\WishlistItemBundleCreatorServiceInterface
   */
  private $bundleCreator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('better_wishlist.bundle_creator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, WishlistItemBundleCreatorServiceInterface $bundle_creator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleCreator = $bundle_creator;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'better_wishlist_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('better_wishlist.settings');
    $entity_types = $this->entityTypeManager->getDefinitions();
    $type_options = [];
    $existing_bundles = [];
    foreach ($entity_types as $entity_type) {
      if ($this->bundleCreator->bundleExists($entity_type->id())) {
        $existing_bundles[] = $entity_type->id();
      }
      $type_options[$entity_type->id()] = $entity_type->getLabel();
    }

    asort($type_options);

    $form['listable_entity_types'] = [
      '#type' => 'checkboxes',
      '#label' => $this->t('Entity types in better_wishlist'),
      '#options' => $type_options,
      '#default_value' => $config->get('listable_entity_types') ?? [],
    ];

    foreach ($existing_bundles as $existing_bundle) {
      $form['listable_entity_types'][$existing_bundle]['#attributes']['disabled'] = 'disabled';
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $listable_entity_types = $form_state->getValue('listable_entity_types');

    $this->config('better_wishlist.settings')
      ->set('listable_entity_types', $listable_entity_types)
      ->save();

    foreach ($listable_entity_types as $listable_entity_type) {
      if ($listable_entity_type !== 0 && !$this->bundleCreator->bundleExists($listable_entity_type)) {
        $this->bundleCreator->createBundle($listable_entity_type);
      }
    }

    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['better_wishlist.settings'];
  }

}
