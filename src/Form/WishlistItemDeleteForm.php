<?php

namespace Drupal\better_wishlist\Form;

use Drupal\better_wishlist\Entity\WishlistInterface;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting Wishlist item entities.
 *
 * @ingroup better_wishlist
 */
class WishlistItemDeleteForm extends ContentEntityDeleteForm {

  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    /** @var \Drupal\better_wishlist\Entity\WishlistItemInterface $entity */
    $entity = $this->getEntity();
    $wishlist = $entity->getWishlist();
    if ($wishlist instanceof WishlistInterface) {
      $wishlist->removeFromWishlist($entity);
      $wishlist->save();
    }
  }

}
