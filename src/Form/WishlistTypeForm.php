<?php

namespace Drupal\better_wishlist\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for wishlist item types..
 */
class WishlistTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $better_wishlist_item_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $better_wishlist_item_type->label(),
      '#description' => $this->t("Label for the Wishlist item type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $better_wishlist_item_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\better_wishlist\Entity\WishlistItemType::load',
      ],
      '#disabled' => !$better_wishlist_item_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $better_wishlist_item_type = $this->entity;
    $status = $better_wishlist_item_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Wishlist item type.', [
          '%label' => $better_wishlist_item_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Wishlist item type.', [
          '%label' => $better_wishlist_item_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($better_wishlist_item_type->toUrl('collection'));
  }

}
