<?php

namespace Drupal\better_wishlist\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting Wishlist entities.
 *
 * @ingroup better_wishlist
 */
class WishlistDeleteForm extends ContentEntityDeleteForm {

  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    /** @var \Drupal\better_wishlist\Entity\WishlistInterface $entity */
    $entity = $this->getEntity();
    $wishlist_items = $entity->get('better_wishlist_items')
      ->referencedEntities();
    if (!empty($wishlist_items)) {
      $storage_handler = $this->entityTypeManager->getStorage('better_wishlist_item');
      $storage_handler->delete($wishlist_items);
    }
  }

}
