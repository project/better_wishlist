<?php

namespace Drupal\better_wishlist;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the storage handler class for Wishlist item entities.
 *
 * This extends the base storage class, adding required special handling for
 * Wishlist item entities.
 *
 * @ingroup better_wishlist
 */
interface WishlistItemStorageInterface extends ContentEntityStorageInterface {

}
