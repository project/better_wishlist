<?php

namespace Drupal\better_wishlist;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for better_wishlist_item.
 */
class WishlistItemTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
