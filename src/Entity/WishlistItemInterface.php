<?php

namespace Drupal\better_wishlist\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Wishlist item entities.
 *
 * @ingroup better_wishlist
 */
interface WishlistItemInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Wishlist item name.
   *
   * @return string
   *   Name of the Wishlist item.
   */
  public function getName();

  /**
   * Sets the Wishlist item name.
   *
   * @param string $name
   *   The Wishlist item name.
   *
   * @return \Drupal\better_wishlist\Entity\WishlistItemInterface
   *   The called Wishlist item entity.
   */
  public function setName($name);

  /**
   * Gets the Wishlist item creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Wishlist item.
   */
  public function getCreatedTime();

  /**
   * Sets the Wishlist item creation timestamp.
   *
   * @param int $timestamp
   *   The Wishlist item creation timestamp.
   *
   * @return \Drupal\better_wishlist\Entity\WishlistItemInterface
   *   The called Wishlist item entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the wishlist entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity.
   */
  public function getWishlist(): ?EntityInterface;

  /**
   * Returns the wishlisted entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity.
   */
  public function getWishlistedItem(): ?EntityInterface;

  /**
   * Adds the given entity to the wishlist item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to wishlist.
   */
  public function setWishlistedItem(EntityInterface $entity): void;

}
