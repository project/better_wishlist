<?php

namespace Drupal\better_wishlist\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Wishlist entities.
 */
class WishlistViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
