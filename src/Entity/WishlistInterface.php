<?php

namespace Drupal\better_wishlist\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Wishlist entities.
 *
 * @ingroup better_wishlist
 */
interface WishlistInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Wishlist name.
   *
   * @return string
   *   Name of the Wishlist.
   */
  public function getName();

  /**
   * Sets the Wishlist name.
   *
   * @param string $name
   *   The Wishlist name.
   *
   * @return \Drupal\better_wishlist\Entity\WishlistInterface
   *   The called Wishlist entity.
   */
  public function setName($name);

  /**
   * Gets the Wishlist creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Wishlist.
   */
  public function getCreatedTime();

  /**
   * Sets the Wishlist creation timestamp.
   *
   * @param int $timestamp
   *   The Wishlist creation timestamp.
   *
   * @return \Drupal\better_wishlist\Entity\WishlistInterface
   *   The called Wishlist entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Adds the given entity to the wishlist.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to wishlist.
   */
  public function addToWishlist(EntityInterface $entity): bool;

  /**
   * Removes the given entity from the wishlist.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to remove from the wishlist.
   */
  public function removeFromWishlist(EntityInterface $entity): bool;

  public function isOnWishlist(EntityInterface $entity): bool;

}
