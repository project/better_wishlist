<?php

namespace Drupal\better_wishlist\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Wishlist entity.
 *
 * @ingroup better_wishlist
 *
 * @ContentEntityType(
 *   id = "better_wishlist",
 *   label = @Translation("Wishlist"),
 *   bundle_label = @Translation("Wishlist type"),
 *   handlers = {
 *     "access" = "\Drupal\entity\EntityAccessControlHandler",
 *     "query_access" = "\Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "storage" = "Drupal\better_wishlist\WishlistStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\better_wishlist\WishlistListBuilder",
 *     "views_data" = "Drupal\better_wishlist\Entity\WishlistViewsData",
 *     "translation" = "Drupal\better_wishlist\WishlistTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\better_wishlist\Form\WishlistForm",
 *       "add" = "Drupal\better_wishlist\Form\WishlistForm",
 *       "edit" = "Drupal\better_wishlist\Form\WishlistForm",
 *       "delete" = "Drupal\better_wishlist\Form\WishlistDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\better_wishlist\WishlistHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "better_wishlist",
 *   data_table = "better_wishlist_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer better_wishlist entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/wishlist/{better_wishlist}",
 *     "add-page" = "/wishlist/add",
 *     "add-form" = "/wishlist/{better_wishlist_type}/add",
 *     "edit-form" = "/wishlist/{better_wishlist}/edit",
 *     "delete-form" =
 *   "/wishlist/{better_wishlist}/delete",
 *     "collection" = "/admin/content/wishlists",
 *   },
 *   bundle_entity_type = "better_wishlist_type",
 *   field_ui_base_route = "entity.better_wishlist_type.edit_form"
 * )
 */
class Wishlist extends ContentEntityBase implements WishlistInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters[$this->getEntityTypeId()] = $this->uuid();
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Wishlist entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Wishlist entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 1000,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['original_wishlist'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Original wishlist'))
      ->setDescription(t('The uuid of the original wishlist this wishlist was cloned from.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 1000,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(false);

    $fields['status']->setDescription(t('A boolean indicating whether the Wishlist is published.'));


    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function addToWishlist(EntityInterface $entity): bool {
    $wishlist_item_storage = $this->entityTypeManager()
      ->getStorage('better_wishlist_item');

    $entity_type = $entity->getEntityTypeId();
    $wishlist_item_bundle_key = $entity_type;

    $bundle = WishlistItemType::load($wishlist_item_bundle_key);
    if (empty($bundle)) {
      throw new \Exception($this->t('Missing wishlist item bundle for :s', [':s' => $entity_type]));
    }
    $found = $this->isOnWishlist($entity);
    if (!$found) {
      $wishlisted_items = $this->get('better_wishlist_items')->referencedEntities();
      /** @var \Drupal\better_wishlist\Entity\WishlistItemInterface $wishlist_item */
      $wishlist_item = $wishlist_item_storage->create(['type' => $wishlist_item_bundle_key]);
      $wishlist_item->setWishlistedItem($entity);
      $wishlist_item->save();
      $wishlisted_items[] = $wishlist_item;
      $this->set('better_wishlist_items', $wishlisted_items);
    }

    return !$found;
  }

  /**
   * {@inheritdoc}
   */
  public function removeFromWishlist(EntityInterface $entity): bool {
    $wishlist_items = $this->get('better_wishlist_items')
      ->referencedEntities();

    $removed = FALSE;
    foreach ($wishlist_items as $key => $wishlist_item) {
      $wishlisted_item = $wishlist_item->getWishlistedItem();
      if (!empty($wishlisted_item)) {
        if ($wishlisted_item->getEntityTypeId() == $entity->getEntityTypeId() && $wishlisted_item->id() == $entity->id()) {
          unset($wishlist_items[$key]);
          $removed = TRUE;
        }
      }
    }
    $this->set('better_wishlist_items', $wishlist_items);
    return $removed;
  }

  public function isOnWishlist(EntityInterface $entity): bool {
    $wishlisted_items = $this->get('better_wishlist_items')
      ->referencedEntities();
    foreach ($wishlisted_items as $key => $wishlist_item) {
      $wishlisted_item = $wishlist_item->getWishlistedItem();
      if (!empty($wishlisted_item)) {
        if ($wishlisted_item->getEntityTypeId() == $entity->getEntityTypeId() && $wishlisted_item->id() == $entity->id()) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

}
