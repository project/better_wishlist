<?php

namespace Drupal\better_wishlist\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Wishlist item type entity.
 *
 * @ConfigEntityType(
 *   id = "better_wishlist_type",
 *   label = @Translation("Wishlist type"),
 *   handlers = {
 *     "view__typebuilder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\better_wishlist\WishlistTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\better_wishlist\Form\WishlistTypeForm",
 *       "edit" = "Drupal\better_wishlist\Form\WishlistTypeForm",
 *       "delete" = "Drupal\better_wishlist\Form\WishlistTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\better_wishlist\WishlistTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "better_wishlist_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "better_wishlist",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/better_wishlist_type/{better_wishlist_type}",
 *     "add-form" = "/admin/structure/better_wishlist_type/add",
 *     "edit-form" = "/admin/structure/better_wishlist_type/manage/{better_wishlist_type}",
 *     "delete-form" = "/admin/structure/better_wishlist_type/manage/{better_wishlist_type}/delete",
 *     "collection" = "/admin/structure/better_wishlist_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   }
 * )
 */
class WishlistType extends ConfigEntityBundleBase implements WishlistTypeInterface {

  /**
   * The Wishlist item type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Wishlist item type label.
   *
   * @var string
   */
  protected $label;

}
