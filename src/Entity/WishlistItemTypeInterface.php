<?php

namespace Drupal\better_wishlist\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Wishlist item type entities.
 */
interface WishlistItemTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
