<?php

namespace Drupal\better_wishlist\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Wishlist item entity.
 *
 * @ingroup better_wishlist
 *
 * @ContentEntityType(
 *   id = "better_wishlist_item",
 *   label = @Translation("Wishlist item"),
 *   bundle_label = @Translation("Wishlist item type"),
 *   handlers = {
 *     "storage" = "Drupal\better_wishlist\WishlistItemStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\better_wishlist\WishlistItemListBuilder",
 *     "views_data" = "Drupal\better_wishlist\Entity\WishlistItemViewsData",
 *     "translation" = "Drupal\better_wishlist\WishlistItemTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\better_wishlist\Form\WishlistItemForm",
 *       "add" = "Drupal\better_wishlist\Form\WishlistItemForm",
 *       "edit" = "Drupal\better_wishlist\Form\WishlistItemForm",
 *       "delete" = "Drupal\better_wishlist\Form\WishlistItemDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\better_wishlist\WishlistItemHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\better_wishlist\WishlistItemAccessControlHandler",
 *   },
 *   base_table = "better_wishlist_item",
 *   data_table = "better_wishlist_item_field_data",
 *   revision_table = "better_wishlist_item_revision",
 *   revision_data_table = "better_wishlist_item_field_revision",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer better_wishlist item entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/structure/better_wishlist_item/{better_wishlist_item}",
 *     "add-page" = "/admin/structure/better_wishlist_item/add",
 *     "add-form" =
 *   "/admin/structure/better_wishlist_item/add/{better_wishlist_item_type}",
 *     "edit-form" =
 *   "/admin/structure/better_wishlist_item/{better_wishlist_item}/edit",
 *     "delete-form" =
 *   "/admin/structure/better_wishlist_item/{better_wishlist_item}/delete",
 *     "collection" = "/admin/structure/better_wishlist_item",
 *   },
 *   bundle_entity_type = "better_wishlist_item_type",
 *   field_ui_base_route = "entity.better_wishlist_item_type.edit_form"
 * )
 */
class WishlistItem extends ContentEntityBase implements WishlistItemInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    $wishlisted_item = $this->getWishlistedItem();
    if (!empty($wishlisted_item)) {
      $this->setName($wishlisted_item->label());
    }
    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  public function label() {
    $item = $this->getWishlistedItem();
    if (!empty($item)) {
      return $item->label();
    }
    else {
      return t('No wishlisted entity.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getWishlist(): ?EntityInterface {
    $wishlist = $this->entityTypeManager()
      ->getListBuilder('better_wishlist')
      ->getStorage()
      ->loadByProperties([
          'better_wishlist_items' => $this->id(),
        ]
      );
    return reset($wishlist);
  }

  /**
   * {@inheritdoc}
   */
  public function getWishlistedItem(): ?EntityInterface {
    $bundle = $this->bundle();
    $linked_entity_type = str_replace('item_', '', $bundle);
    $field_name = $linked_entity_type;
    if ($this->hasField($field_name) && !$this->get($field_name)->isEmpty()) {
      return $this->get($field_name)->referencedEntities()[0];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Wishlist item entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Wishlist item entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 1000,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Wishlist item is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function setWishlistedItem(EntityInterface $entity): void {
    $field_name = $entity->getEntityTypeId();
    if ($this->hasField($field_name)) {
      $this->set($field_name, $entity);
    }
  }

}
