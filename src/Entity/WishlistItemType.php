<?php

namespace Drupal\better_wishlist\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Wishlist item type entity.
 *
 * @ConfigEntityType(
 *   id = "better_wishlist_item_type",
 *   label = @Translation("Wishlist item type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\better_wishlist\WishlistItemTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\better_wishlist\Form\WishlistItemTypeForm",
 *       "edit" = "Drupal\better_wishlist\Form\WishlistItemTypeForm",
 *       "delete" = "Drupal\better_wishlist\Form\WishlistItemTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\better_wishlist\WishlistItemTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "better_wishlist_item_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "better_wishlist_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/better_wishlist_item_type/{better_wishlist_item_type}",
 *     "add-form" = "/admin/structure/better_wishlist_item_type/add",
 *     "edit-form" = "/admin/structure/better_wishlist_item_type/{better_wishlist_item_type}/edit",
 *     "delete-form" = "/admin/structure/better_wishlist_item_type/{better_wishlist_item_type}/delete",
 *     "collection" = "/admin/structure/better_wishlist_item_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   }
 * )
 */
class WishlistItemType extends ConfigEntityBundleBase implements WishlistItemTypeInterface {

  /**
   * The Wishlist item type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Wishlist item type label.
   *
   * @var string
   */
  protected $label;

}
