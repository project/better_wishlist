<?php

/**
 * @file
 * Contains better_wishlist.page.inc.
 *
 * Page callback for Wishlist entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Wishlist templates.
 *
 * Default template: better_wishlist.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_better_wishlist(array &$variables) {
  // Fetch Wishlist Entity Object.
  $better_wishlist = $variables['elements']['#better_wishlist'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
