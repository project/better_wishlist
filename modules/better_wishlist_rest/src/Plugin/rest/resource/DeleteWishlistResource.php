<?php

namespace Drupal\better_wishlist_rest\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource for wishlists.
 *
 * @RestResource(
 *   id = "better_wishlist_delete",
 *   label = @Translation("Better wishlist delete"),
 *   uri_paths = {
 *     "create" = "/better-wishlist/delete"
 *   }
 * )
 */
class DeleteWishlistResource extends WishlistResourceBase {

  public function post(Request $request) {
    $uuid = $this->getPostData($request, 'uuid');
    if (empty($uuid)) {
      throw new \Exception($this->t('Missing argument uuid!'));
    }

    $wishlists = $this->entityTypeManager->getStorage('better_wishlist')
      ->loadByProperties(['uuid' => $uuid]);

    foreach ($wishlists as $wishlist) {
      $wishlist->delete();
    }

    return new ModifiedResourceResponse([
      'success' => 'true',
    ]);

  }

}
