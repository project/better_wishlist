<?php

namespace Drupal\better_wishlist_rest\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource for wishlists.
 *
 * @RestResource(
 *   id = "better_wishlist_create",
 *   label = @Translation("Better wishlist create"),
 *   uri_paths = {
 *     "create" = "/better-wishlist/create"
 *   }
 * )
 */
class CreateWishlistResource extends WishlistResourceBase {

  public function post(Request $request) {
    $name = $this->getPostData($request, 'name');
    $type = $this->getPostData($request, 'type');
    if (empty($type)) {
      $type = 'default';
    }
    if (empty($name)) {
      throw new \Exception($this->t('Missing argument name!'));
    }

    $wishlist = $this->entityTypeManager->getStorage('better_wishlist')
      ->create(['name' => $name, 'type' => $type]);
    $wishlist->save();
    return new ModifiedResourceResponse([
      'uuid' => $wishlist->uuid(),
      'name' => $wishlist->label(),
    ]);

  }

}
