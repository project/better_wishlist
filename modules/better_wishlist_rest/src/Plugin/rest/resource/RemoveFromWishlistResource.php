<?php

namespace Drupal\better_wishlist_rest\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource for wishlists.
 *
 * @RestResource(
 *   id = "better_wishlist_remove_from",
 *   label = @Translation("Better wishlist remove_from"),
 *   uri_paths = {
 *     "create" = "/better-wishlist/remove-from"
 *   }
 * )
 */
class RemoveFromWishlistResource extends WishlistResourceBase {

  public function post(Request $request) {
    $uuid = $this->getPostData($request, 'uuid');
    $entity_type = $this->getPostData($request, 'entity_type');
    $entity_id = $this->getPostData($request, 'entity_id');
    if (empty($uuid) || empty($entity_type) || empty($entity_id)) {
      throw new \Exception($this->t('Missing arguments name, entity_type, entity_id!'));
    }

    /** @var \Drupal\better_wishlist\Entity\WishlistInterface[] $wishlists */
    $wishlists = $this->entityTypeManager->getStorage('better_wishlist')
      ->loadByProperties(['uuid' => $uuid]);

    $success = FALSE;
    if (!empty($wishlists)) {
      $wishlist = current($wishlists);
      $success = $wishlist->removeFromWishlist($this->entityTypeManager->getStorage($entity_type)
        ->load($entity_id));
      $wishlist->save();
    }

    return new ModifiedResourceResponse([
      'success' => $success,
    ]);

  }

}
