<?php

namespace Drupal\better_wishlist_rest\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource for wishlists.
 *
 * @RestResource(
 *   id = "better_wishlist_wishlists",
 *   label = @Translation("Better wishlist wishlists"),
 *   uri_paths = {
 *     "canonical" = "/better-wishlist/wishlists"
 *   }
 * )
 */
class WishlistsResource extends WishlistResourceBase {

  /**
   * {@inheritdoc}
   */
  public function get(Request $request) {
    $uuid_string = $request->get('uuids');
    $r = [];
    if ($this->currentUser->isAuthenticated()) {
      $wishlists = $this->entityTypeManager->getStorage('better_wishlist')
        ->loadByProperties(['user_id' => $this->currentUser->id()]);
    }
    elseif (!empty($uuid_string)) {
      $uuids = explode(' ', $uuid_string);

      /** @var \Drupal\better_wishlist\Entity\WishlistInterface[] $wishlists */
      $wishlists = $this->entityTypeManager->getStorage('better_wishlist')
        ->loadByProperties(['uuid' => $uuids]);
    }

    if (!empty($wishlists)) {
      foreach ($wishlists as $wishlist) {

        if ($wishlist->getOwner()->id() != $this->currentUser->id()) {
          continue;
        }

        $row = [
          'uuid' => $wishlist->uuid(),
          'name' => $wishlist->label(),
        ];
        /** @var \Drupal\better_wishlist\Entity\WishlistItemInterface[] $items */
        $items = $wishlist->get('better_wishlist_items')->referencedEntities();
        foreach ($items as $item) {
          $entity = $item->getWishlistedItem();
          if (!empty($entity)) {
            $row['items'][] = [
              'entity_type' => $entity->getEntityTypeId(),
              'entity_id' => $entity->id(),
            ];
          }

        }
        $r[] = $row;
      }
    }

    return new ModifiedResourceResponse($r);
  }

}
