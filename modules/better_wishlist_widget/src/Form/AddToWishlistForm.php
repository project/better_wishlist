<?php

namespace Drupal\better_wishlist_widget\Form;

use Drupal\better_wishlist_widget\WidgetLazyBuilder;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AddToWishlistForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, RouteMatchInterface $route_match, RendererInterface $renderer) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->routeMatch = $route_match;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'better_wishlist_widget_add_to_wishlist_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity_type = $this->routeMatch->getParameter('entity_type');
    $entity_id = $this->routeMatch->getParameter('entity_id');

    $wishlist_options = $this->getWishlistOptions($this->entityTypeManager->getStorage($entity_type)
      ->load($entity_id));

    $form['messages'] = [
      '#type' => 'markup',
      '#markup' => '<div class="modal-messages"></div>',
    ];

    $bundles = $this->entityTypeManager->getStorage('better_wishlist_type')
      ->loadMultiple();

    if (empty($bundles)) {
      $form['messages']['#markup'] = $this->t('No wishlist types configured.');
      return $form;
    }

    $form['entity_type'] = [
      '#type' => 'value',
      '#value' => $entity_type,
    ];

    $form['entity_id'] = [
      '#type' => 'value',
      '#value' => $entity_id,
    ];

    $mode_options = [];
    if (!empty($wishlist_options['options'])) {
      $mode_options['add'] = $this->t('Add to existing wishlist');
    }
    $mode_options['create'] = $this->t('Create new wishlist');


    $form['mode'] = [
      '#type' => 'radios',
      '#options' => $mode_options,
      '#default_value' => empty($wishlist_options['options']) ? 'create' : 'add',
    ];

    $form['create'] = [
      '#type' => 'container',
      '#title' => $this->t('Create new wishlist'),
      '#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'create'],
        ],
        'invisible' => [
          ':input[name="mode"]' => ['value' => 'add'],
        ],
      ],
    ];

    $form['create']['new_wishlist'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New wishlist'),
      '#states' => [
        'required' => [
          ':input[name="mode"]' => ['value' => 'create'],
        ],
      ],
    ];

    if (count($bundles) > 1) {
      $bundle_options = [];

      foreach ($bundles as $bundle) {
        $bundle_options[$bundle->id()] = $bundle->label();
      }
      $form['create']['bundle'] = [
        '#type' => 'radios',
        '#title' => $this->t('Type'),
        '#options' => $bundle_options,
        '#required' => TRUE,
      ];
    }
    else {
      $bundle = current($bundles);
      $form['create']['bundle'] = [
        '#type' => 'value',
        '#value' => $bundle->id(),
      ];
    }


    $form['add'] = [
      '#type' => 'container',
      '#title' => $this->t('Add to existing wishlist'),
      '#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'add'],
        ],
        'invisible' => [
          ':input[name="mode"]' => ['value' => 'create'],
        ],
      ],
    ];

    $form['add']['existing_wishlists'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Wishlists'),
      '#options' => $wishlist_options['options'],
      '#default_value' => $wishlist_options['default_value'],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => [$this, 'ajaxSubmit'],
      ],
    ];

    return $form;
  }

  /**
   * Loads all wishlists of the current user and returns them as fapi options.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check for.
   *
   * @return array
   *   The wishlists as fapi options.
   */
  protected function getWishlistOptions(EntityInterface $entity) {
    /** @var \Drupal\better_wishlist\Entity\WishlistInterface[] $wishlists */
    $wishlists = $this->entityTypeManager->getStorage('better_wishlist')
      ->loadByProperties([
        'user_id' => $this->currentUser->id(),
      ]);

    $options = ['options' => [], 'default_value' => []];
    foreach ($wishlists as $wishlist) {
      $options['options'][$wishlist->id()] = $wishlist->label();
      if ($wishlist->isOnWishlist($entity)) {
        $options['default_value'][] = $wishlist->id();
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $entity = $this->entityTypeManager->getStorage($form_state->getValue('entity_type'))
      ->load($form_state->getValue('entity_id'));
    switch ($form_state->getValue('mode')) {
      case 'add':
        $existing_wishlists = $form_state->getValue('existing_wishlists');
        foreach ($existing_wishlists as $wishlist_id => $status) {
          /** @var \Drupal\better_wishlist\Entity\WishlistInterface $wishlist */
          $wishlist = $this->entityTypeManager->getStorage('better_wishlist')
            ->load($wishlist_id);
          if ($status !== 0) {
            $wishlist->addToWishlist($entity);
          }
          else {
            $wishlist->removeFromWishlist($entity);
          }
          $wishlist->save();
        }
        break;
      case 'create':
        $wishlist = $this->entityTypeManager->getStorage('better_wishlist')
          ->create([
            'name' => $form_state->getValue('new_wishlist'),
            'type' => $form_state->getValue('bundle'),
            'uid' => $this->currentUser->id(),
          ]);
        $wishlist->addToWishlist($entity);
        $wishlist->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getValue('mode') == 'create' && $form_state->getValue('new_wishlist') == '') {
      $form_state->setErrorByName('new_wishlist', $this->t('%field field is required.', ['%field' => $form['create']['new_wishlist']['#title']]));
    }

    $wishlists = array_filter($form_state->getValue('existing_wishlists'));
    if ($form_state->getValue('mode') == 'add' && empty($wishlists)) {
      $form_state->setErrorByName('existing_wishlists', $this->t('%field field is required.', ['%field' => $form['add']['existing_wishlists']['#title']]));
    }
  }

  /**
   * AJAX submit handler.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response.
   */
  public function ajaxSubmit(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $errors = $form_state->getErrors();
    if (!empty($errors)) {
      $messages = [
        '#theme' => 'status_messages',
        '#message_list' => \Drupal::messenger()->deleteAll(),
      ];
      $response->addCommand(new ReplaceCommand('.modal-messages', $this->renderer->render($messages)));
    }
    else {
      $entity_type = $form_state->getValue('entity_type');
      $entity_id = $form_state->getValue('entity_id');
      $link_array = WidgetLazyBuilder::build($entity_type, $entity_id);
      $link = $this->renderer->render($link_array);
      $response->addCommand(new CloseDialogCommand());
      $response->addCommand(new ReplaceCommand('#add-to-wishlist-' . $entity_type . '-' . $entity_id, $link));
    }

    return $response;
  }

}
