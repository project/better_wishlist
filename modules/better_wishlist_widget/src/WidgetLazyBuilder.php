<?php

namespace Drupal\better_wishlist_widget;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;

/**
 * Class for the widget lazy builder.
 */
class WidgetLazyBuilder implements TrustedCallbackInterface {

  /**
   * Builds the add to wishlist link.
   *
   * @param $entity_type
   *   The entity type.
   * @param $entity_id
   *   The entity id.
   *
   * @return array
   *   The build array.
   */
  public static function build($entity_type, $entity_id) {

    if (!\Drupal::currentUser()
      ->hasPermission('create better_wishlist')) {
      return [];
    }

    /** @var \Drupal\better_wishlist\Entity\WishlistInterface[] $wishlists */
    $wishlists = \Drupal::entityTypeManager()
      ->getStorage('better_wishlist')
      ->loadByProperties([
        'user_id' => \Drupal::currentUser()->id(),
      ]);

    $entity = \Drupal::entityTypeManager()
      ->getStorage($entity_type)
      ->load($entity_id);

    $wishlisted = FALSE;

    foreach ($wishlists as $wishlist) {
      if ($wishlist->isOnWishlist($entity)) {
        $wishlisted = TRUE;
        break;
      }
    }

    $class = $wishlisted ? 'on-wishlist' : 'not-on-wishlist';

    $link = Url::fromRoute('better_wishlist_widget.add_to_wishlist_form', [
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
    ]);
    $markup = [
      '#theme' => 'better_wishlist_widget',
      '#url' => $link,
      '#title' => t('Add to wishlist'),
      '#attributes' => [
        'class' => ['use-ajax', $class],
        'data-dialog-options' => '{"width":600}',
        'data-dialog-type' => 'modal',
        'id' => 'add-to-wishlist-' . $entity_type . '-' . $entity_id,
      ],
    ];

    return [
      '#markup' => \Drupal::service('renderer')->render($markup),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['build'];
  }

}
